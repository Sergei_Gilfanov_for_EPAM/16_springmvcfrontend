define(["app/pages/client-0.1", "app/pages/clientList-0.1"], function (client, clientList) {
	return {
		clientList: clientList,
		client: client
	}
});
