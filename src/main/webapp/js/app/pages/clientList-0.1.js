define(["jquery", "app/popups-0.1"], function ($, popups) {
	var clientListPageElement = $('.client-list-page');
	var clientListElement = $('.client-list')

	clientListPageElement.find('.add-client-button').click(function() {
		popups.clientAdd.show()
		.done(function(newClient) {
			document.location.reload(true);
		});
	});
});
