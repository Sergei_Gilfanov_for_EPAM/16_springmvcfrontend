// Place third party dependencies in the lib folder
//
// Configure loading modules from the lib directory,
// except 'app' ones,
requirejs.config({
	"baseUrl": "/store/js/vendor",
	"paths": {
		"app": "../app",
		"jquery": "https://ajax.googleapis.com/ajax/libs/jquery/2.2.4/jquery.min",
		"urijs": "https://cdnjs.cloudflare.com/ajax/libs/URI.js/1.18.1"
	}
});
// Load the main app module to start the app
requirejs(["app/main-0.1"]);
