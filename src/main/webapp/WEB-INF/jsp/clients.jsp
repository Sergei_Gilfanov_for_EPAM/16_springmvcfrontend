<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ page contentType="text/html; charset=utf-8" pageEncoding="UTF-8" %>
<c:set var="baseUrl" value="${pageContext.request.contextPath}" />

<!doctype html>
<html lang="ru">
<head>
	<meta charset="utf-8">
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8">

	<title></title>
	<meta name="description" content="">
	<meta name="author" content="">

	<link rel="stylesheet" href="${baseUrl}/css/palette.css">
	<link rel="stylesheet" href="${baseUrl}/css/styles-0.1.css">
</head>
<body>
	<div class="page-header">
		<span class="page-header_left"></span>
		<span class="page-header_right"><span>${currentUser.fullName}</span>
		<a href="${baseUrl}/logout" class="page-header_logout-button">Выход</a></span>
	</div>
	<div class="client-list-page app-page">
		<h1>Список клиентов</h1>
		<p>
			<ul class="client-list">
			<c:forEach var="client" items="${clients}">
				<li class="client-list-item">
					<a class="client-name" href="${baseUrl}/clients/${client.id}/orders"><c:out value="${client.clientName}"/></a>
				</li>
			</ul>
			</c:forEach>
			<div class="button-container"><div class="add-client-button button">Добавить клиента</div></div>
		</p>
	</div>

	<div class="popup-overlay hidden">
		<div class="client-add app-popup hidden">
			<span class="close-button"></span>
			<form>
				<label>Имя нового клиента: <input type="text" name="clientName"/></label>
				<div class="button-container"><div class="button">Сохранить</div></div>
			</form>
		</div>
	</div>
	<script data-main="${baseUrl}/js/app" src="${baseUrl}/js/vendor/require.js"></script>

</body>
</html>
