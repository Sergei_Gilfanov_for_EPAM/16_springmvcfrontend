package com.epam.javatraining2016.springmvcfrontend.controller;

import java.io.IOException;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.SessionAttribute;
import org.springframework.web.bind.annotation.SessionAttributes;
import org.springframework.web.servlet.ModelAndView;

import com.epam.javatraining2016.springmvcfrontend.protocol.jaxws.Client;
import com.epam.javatraining2016.springmvcfrontend.protocol.jaxws.Order;
import com.epam.javatraining2016.springmvcfrontend.protocol.jaxws.OrdersPaged;
import com.epam.javatraining2016.springmvcfrontend.protocol.jaxws.UserSearchResult;
import com.epam.javatraining2016.springmvcfrontend.support.CurrentUser;
import com.epam.javatraining2016.springmvcfrontend.support.SoapUser;

@Controller
@SessionAttributes("currentUser")
@RequestMapping(value = "/clients/{clientId}/orders")
public class Orders implements SoapUser {
  // private static final Logger log = LoggerFactory.getLogger(Clients.class);

  @GetMapping
  public ModelAndView get(@PathVariable int clientId, @RequestParam(required = false) Integer from,
      @RequestParam(required = false) Integer statusId, HttpSession httpSession) {
    final int pageLength = 5;
    ModelAndView mav = new ModelAndView();
    Object currentUser = httpSession.getAttribute("currentUser");
    if (currentUser == null) {
      mav.setViewName("redirect:/login");
      return mav;
    }
    mav.addObject("clientId", clientId);

    OrdersPaged ordersPaged;
    if (from == null) {
      ordersPaged = soapEndPoint().getOrdersPaged(clientId, -1, pageLength, statusId);
    } else {
      ordersPaged = soapEndPoint().getOrdersPaged(clientId, from, pageLength, statusId);
    }
    mav.addObject("ordersPaged", ordersPaged);

    if (statusId != null) {
      mav.addObject("statusId", String.format("statusId=%d&", statusId));
    }

    mav.setViewName("orders");
    return mav;
  }
}
