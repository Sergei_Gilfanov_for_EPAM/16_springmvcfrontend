package com.epam.javatraining2016.springmvcfrontend.controller;

import java.io.IOException;

import javax.servlet.http.HttpSession;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.SessionAttributes;
import org.springframework.web.servlet.ModelAndView;

import com.epam.javatraining2016.springmvcfrontend.protocol.jaxws.UserSearchResult;
import com.epam.javatraining2016.springmvcfrontend.support.CurrentUser;
import com.epam.javatraining2016.springmvcfrontend.support.SoapUser;

@Controller
@SessionAttributes("currentUser")
@RequestMapping(value = {"", "/login"})
public class Login implements SoapUser {

  @GetMapping
  public ModelAndView get() {
    ModelAndView mav = new ModelAndView();
    mav.setViewName("login");
    return mav;
  }

  @PostMapping
  public ModelAndView post(@RequestParam String login, @RequestParam String password) {
    ModelAndView mav = new ModelAndView();
    mav.setViewName("login");

    // TODO: проверки и сообщения об ошибках

    UserSearchResult userSearchResult = soapEndPoint().login(login, password);

    if (!userSearchResult.isFound()) {
      mav.addObject("userNotFound", true);
      return mav;
    }

    CurrentUser currentUser = new CurrentUser(userSearchResult.getUser().getLogin(),
        userSearchResult.getUser().getFullName());
    // session.setAttribute("currentUser", currentUser);
    mav.addObject("currentUser", currentUser);
    mav.setViewName("redirect:/clients");
    return mav;
  }

}
