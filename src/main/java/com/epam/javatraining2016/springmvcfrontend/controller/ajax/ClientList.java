package com.epam.javatraining2016.springmvcfrontend.controller.ajax;

import java.util.ArrayList;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.epam.javatraining2016.springmvcfrontend.protocol.Client;
import com.epam.javatraining2016.springmvcfrontend.support.SoapUser;



@RestController
@RequestMapping(value = "/backend/clientList")
public class ClientList implements SoapUser {
  // private static final Logger log = LoggerFactory.getLogger(ClientList.class);

  @RequestMapping(method = RequestMethod.GET)
  public List<Client> get() {
    List<com.epam.javatraining2016.springmvcfrontend.protocol.jaxws.Client> soapClients =
        soapEndPoint().getClients();

    List<Client> retval = new ArrayList<Client>(soapClients.size());
    for (com.epam.javatraining2016.springmvcfrontend.protocol.jaxws.Client soapClient : soapClients) {
      Client client = new Client(soapClient.getId(), soapClient.getClientName());
      retval.add(client);
    }
    return retval;
  }
}
