package com.epam.javatraining2016.springmvcfrontend.controller;

import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.SessionAttributes;
import org.springframework.web.servlet.ModelAndView;

import com.epam.javatraining2016.springmvcfrontend.protocol.jaxws.UserSearchResult;
import com.epam.javatraining2016.springmvcfrontend.support.CurrentUser;
import com.epam.javatraining2016.springmvcfrontend.support.PasswordsMismatchException;
import com.epam.javatraining2016.springmvcfrontend.support.SoapUser;

@Controller
@SessionAttributes("currentUser")
@RequestMapping(value = "/registration")
public class Registration implements SoapUser {

  @GetMapping
  public ModelAndView get() {
    ModelAndView mav = new ModelAndView();
    mav.setViewName("registration");
    return mav;
  }

  @PostMapping
  public ModelAndView post(@RequestParam String login, @RequestParam String fullName,
      @RequestParam String password, @RequestParam String password2) {

    if (!password.equals(password2)) {
      throw new PasswordsMismatchException();
    }

    soapEndPoint().createUser(login, fullName, password2);
    UserSearchResult userSearchResult = soapEndPoint().login(login, password2);

    ModelAndView mav = new ModelAndView();
    if (!userSearchResult.isFound()) {
      mav.setViewName("login");
      mav.addObject("userNotFound", true);
      return mav;
    }

    CurrentUser currentUser = new CurrentUser(userSearchResult.getUser().getLogin(),
        userSearchResult.getUser().getFullName());
    mav.addObject("currentUser", currentUser);
    mav.setViewName("redirect:/clients");
    return mav;
  }

}
