package com.epam.javatraining2016.springmvcfrontend.protocol;

public class Client {
  public int id;
  public String clientName;
  
  public Client(int id, String clientName) {
    super();
    this.id = id;
    this.clientName = clientName;
  }
}
