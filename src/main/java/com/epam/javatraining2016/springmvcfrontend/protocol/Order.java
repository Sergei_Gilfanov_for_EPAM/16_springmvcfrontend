package com.epam.javatraining2016.springmvcfrontend.protocol;

public class Order {
  public int id;
  public String status;

  public Order(int id, String status) {
    super();
    this.id = id;
    this.status = status;
  }
}
