package com.epam.javatraining2016.springmvcfrontend.support;

public class CurrentUser {
  private final String login;
  private final String fullName;

  public CurrentUser() {
    login = null;
    fullName = null;
  }

  public CurrentUser(String login, String fullName) {
    this.login = login;
    this.fullName = fullName;
  }

  public String getLogin() {
    return login;
  }

  public String getFullName() {
    return fullName;
  }
}
