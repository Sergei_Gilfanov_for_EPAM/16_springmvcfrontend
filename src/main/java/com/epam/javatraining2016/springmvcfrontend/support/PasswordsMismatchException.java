package com.epam.javatraining2016.springmvcfrontend.support;

import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.http.HttpStatus;


@ResponseStatus(value=HttpStatus.BAD_REQUEST, reason="{reason: 'password != password2'}")
public class PasswordsMismatchException extends RuntimeException {
  private static final long serialVersionUID = -5949885456100000951L;
}
